# Partie 3 : Serveur web

## 2. Install

### 🌞 Installez NGINX

```
[baptiste@web ~]$ sudo dnf install nginx
[sudo] password for baptiste:

Installed:
  nginx-1:1.20.1-13.el9.x86_64                nginx-core-1:1.20.1-13.el9.x86_64        nginx-filesystem-1:1.20.1-13.el9.noarch
  rocky-logos-httpd-90.13-1.el9.noarch

Complete!

```

## 3. Analyse

```
[baptiste@web ~]$ sudo systemctl enable nginx
sudo systemctl start nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.

```

```
[baptiste@web nginx]$ sudo systemctl status nginx
[sudo] password for baptiste:
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
     Active: active (running) since Mon 2023-01-02 19:13:46 CET; 16min ago

```

### 🌞 Analysez le service NGINX

```
[baptiste@web ~]$ ps -ef | grep nginx
root       10987       1  0 19:13 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx      10988   10987  0 19:13 ?        00:00:00 nginx: worker process
baptiste   10992     867  0 19:16 pts/0    00:00:00 grep --color=auto nginx
```

```
[baptiste@web ~]$ sudo ss -alnpt | grep nginx
[sudo] password for baptiste:
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=10988,fd=6),("nginx",pid=10987,fd=6))
LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=10988,fd=7),("nginx",pid=10987,fd=7))

```

```
[baptiste@web nginx]$ sudo cat nginx.conf | grep root
        root         /usr/share/nginx/html;
#        root         /usr/share/nginx/html;
```

```
[baptiste@web nginx]$ ls -la
total 84
drwxr-xr-x.  4 root root 4096 Jan  2 19:11 .
drwxr-xr-x. 81 root root 8192 Jan  2 19:11 ..
drwxr-xr-x.  2 root root    6 Oct 31 16:37 conf.d
drwxr-xr-x.  2 root root    6 Oct 31 16:37 default.d
-rw-r--r--.  1 root root 1077 Oct 31 16:37 fastcgi.conf
-rw-r--r--.  1 root root 1077 Oct 31 16:37 fastcgi.conf.default
-rw-r--r--.  1 root root 1007 Oct 31 16:37 fastcgi_params
-rw-r--r--.  1 root root 1007 Oct 31 16:37 fastcgi_params.default
-rw-r--r--.  1 root root 2837 Oct 31 16:37 koi-utf
-rw-r--r--.  1 root root 2223 Oct 31 16:37 koi-win
-rw-r--r--.  1 root root 5231 Oct 31 16:37 mime.types
-rw-r--r--.  1 root root 5231 Oct 31 16:37 mime.types.default
-rw-r--r--.  1 root root 2334 Oct 31 16:37 nginx.conf
-rw-r--r--.  1 root root 2656 Oct 31 16:37 nginx.conf.default
-rw-r--r--.  1 root root  636 Oct 31 16:37 scgi_params
-rw-r--r--.  1 root root  636 Oct 31 16:37 scgi_params.default
-rw-r--r--.  1 root root  664 Oct 31 16:37 uwsgi_params
-rw-r--r--.  1 root root  664 Oct 31 16:37 uwsgi_params.default
-rw-r--r--.  1 root root 3610 Oct 31 16:37 win-utf

```

## 4. Visite du service web

### 🌞 Configurez le firewall pour autoriser le trafic vers le service NGINX

```
[baptiste@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

```
[baptiste@web ~]$ sudo firewall-cmd --zone=public --add-service=https --permanent
success
```

### 🌞 Accéder au site web

```
[baptiste@web ~]$ curl http://10.5.1.11 | head -n 7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
100  7620  100  7620    0     0   354k      0 --:--:-- --:--:-- --:--:--  338k
curl: (23) Failed writing body
```

### 🌞 Vérifier les logs d'accès

```
[baptiste@web log]$ sudo cat /var/log/nginx/access.log | tail -n 3
10.5.1.11 - - [02/Jan/2023:19:43:04 +0100] "GET / HTTP/1.1" 200 7620 "-" "curl/7.76.1" "-"
10.5.1.11 - - [02/Jan/2023:19:43:27 +0100] "GET / HTTP/1.1" 200 7620 "-" "curl/7.76.1" "-"
10.5.1.11 - - [02/Jan/2023:19:56:29 +0100] "GET / HTTP/1.1" 200 7620 "-" "curl/7.76.1" "-"
```

## 5. Modif de la conf du serveur web

### 🌞 Changer le port d'écoute

```
[baptiste@web nginx]$ sudo cat /etc/nginx/nginx.conf | grep 8080
        listen       8080;
```

```
[baptiste@web nginx]$ sudo ss -tulnp | grep 8080
tcp   LISTEN 0      511          0.0.0.0:8080      0.0.0.0:*    users:(("nginx",pid=11123,fd=6),("nginx",pid=11122,fd=6))
```

```
[baptiste@web nginx]$  sudo firewall-cmd --list-all | grep port
  ports: 8080/tcp
  forward-ports:
  source-ports:
```

```
[baptiste@web nginx]$ curl http://10.5.1.11:8080 | head -n 7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
100  7620  100  7620    0     0   496k      0 --:--:-- --:--:-- --:--:--  531k
curl: (23) Failed writing body
```

### 🌞 Changer l'utilisateur qui lance le service

```
[baptiste@web nginx]$  sudo useradd web -m -s /home/web -u 2000

[baptiste@web nginx]$ sudo passwd web
Changing password for user web.
New password: 
Retype new password: 
passwd: all authentication tokens updated successfully.

```

```
[baptiste@web nginx]$ sudo cat nginx.conf | grep web
user web;
```

```
[baptiste@web nginx]$ ps -ef | grep web
web        11277   11276  0 21:04 ?        00:00:00 nginx: worker process
baptiste   11294     867  0 21:06 pts/0    00:00:00 grep --color=auto web
```

### 🌞 Changer l'emplacement de la racine Web

```
[baptiste@web ~]$ sudo cat /var/www/site_web_1/index.html
<h1> je suis un petit papillon</h1>
```

```
[baptiste@web nginx]$ sudo cat /etc/nginx/nginx.conf | grep root
        root         /var/www/site_web_1/;

```


```
[baptiste@web site_web_1]$ sudo curl http://10.5.1.11:8080
<h1> je suis un petit papillon</h1>
```

## 6. Deux sites web sur un seul serveur

### 🌞 Repérez dans le fichier de conf

```
[baptiste@web ~]$ sudo cat /etc/nginx/nginx.conf | grep conf.d
    # Load modular configuration files from the /etc/nginx/conf.d directory.
    include /etc/nginx/conf.d/*.conf;
```

### 🌞 Créez le fichier de configuration pour le premier site

```
[baptiste@web conf.d]$ sudo cat site_web_1.conf
server {
        listen       8080;
        listen       [::]:8080;
        server_name  _;
        root         /var/www/site_web_1/;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
```

### 🌞 Créez le fichier de configuration pour le deuxième site

```
[baptiste@web conf.d]$ sudo cat site_web_2.conf
server {
        listen       8888;
        listen       [::]:8888;
        server_name  _;
        root         /var/www/site_web_2/;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
```

```
[baptiste@web conf.d]$ sudo firewall-cmd --permanent --zone=public --add-port=8888/tcp
success

[baptiste@web conf.d]$ sudo systemctl restart nginx

[baptiste@web conf.d]$ sudo firewall-cmd --reload
success
```

### 🌞 Prouvez que les deux sites sont disponibles

```
[baptiste@web site_web_2]$ curl 10.5.1.11:8080
<h1> je suis un petit papillon</h1>
```

```
[baptiste@web site_web_2]$ curl 10.5.1.11:8888
<h1> Comme on se retrouve ♥ </h1>
```