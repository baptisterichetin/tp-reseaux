# Partie 2 : Serveur de partage de fichiers

## 🌞 Donnez les commandes réalisées sur le serveur NFS storage.tp4.linux

```

[baptiste@storage ~]$ sudo dnf install nano


[baptiste@storage ~]$ sudo nano /etc/exports


[baptiste@storage ~]$ sudo cat /etc/exports
[sudo] password for baptiste:
/storage/site_web_1/ 10.5.1.11(rw,sync,no_subtree_check)
/storage/site_web_2/ 10.5.1.11(rw,sync,no_subtree_check)

```

## 🌞 Donnez les commandes réalisées sur le client NFS web.tp4.linux

```

[baptiste@web ~]$ sudo cat /etc/fstab | grep 10.5.1.10
10.5.1.10:/storage/site_web_1/   /var/www/site_web_1/   nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0
10.5.1.10:/storage/site_web_2/   /var/www/site_web_2/ nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0

```