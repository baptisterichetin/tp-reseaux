# Partie 1 : Partitionnement du serveur de stockage

## 🌞 Partitionner le disque à l'aide de LVM
---
### - créer un physical volume (PV) :

```
[baptiste@storage ~]$ sudo pvcreate /dev/sdb
[sudo] password for baptiste:
  Physical volume "/dev/sdb" successfully created.
```
### Je vérifie :

```
[baptiste@storage ~]$ sudo pvdisplay
  "/dev/sdb" is a new physical volume of "2.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               2.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               43zcdV-z6Kz-c1Np-qFSb-9cRF-cmw6-g5qz8l

```

### - créer un nouveau volume group (VG)

```
[baptiste@storage ~]$ sudo vgcreate storage /dev/sdb
  Volume group "storage" successfully created
```

### Je vérifie :

```
[baptiste@storage ~]$ sudo vgs
  VG      #PV #LV #SN Attr   VSize  VFree
  storage   1   0   0 wz--n- <2.00g <2.00g
```

### - créer un nouveau logical volume (LV) : ce sera la partition utilisable

```
[baptiste@storage ~]$ sudo lvcreate -l 100%FREE storage -n data
  Logical volume "data" created.
```

### Je vérifie :

```
[baptiste@storage ~]$ sudo lvs
  LV   VG      Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  data storage -wi-a----- <2.00g

```

## 🌞 Formater la partition

### - vous formaterez la partition en ext4 (avec une commande mkfs)

```
[baptiste@storage ~]$ mkfs -t ext4 /dev/storage/data
mke2fs 1.46.5 (30-Dec-2021)
mkfs.ext4: Permission denied while trying to determine filesystem size
[baptiste@storage ~]$ sudo mkfs -t ext4 /dev/storage/data
mke2fs 1.46.5 (30-Dec-2021)
Creating filesystem with 523264 4k blocks and 130816 inodes
Filesystem UUID: 6beadacf-7229-48e5-87eb-9f334c28cd11
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

```

## 🌞 Monter la partition

### - montage de la partition (avec la commande mount)

```
[baptiste@storage ~]$ sudo mkdir /mnt/data1
[sudo] password for baptiste:

[baptiste@storage ~]$ sudo mount /dev/storage/data /mnt/data1/
```

### - utilisez un | grep pour isoler les lignes intéressantes

```
[baptiste@storage ~]$ df -h | grep data
/dev/mapper/storage-data  2.0G   24K  1.9G   1% /mnt/data1
```

### - Prouvez que vous pouvez lire et écrire des données sur cette partition

```
[baptiste@storage ~]$ sudo touch /mnt/data1/lost+found/

[baptiste@storage ~]$ ls
teste
```

### - Définir un montage automatique de la partition (fichier /etc/fstab)

```
[baptiste@storage ~]$ cat /etc/fstab | grep storage
/dev/mapper/storage-data /mnt/data1 ext4 defaults 0 0

```

### - vous vérifierez que votre fichier /etc/fstab fonctionne correctement

```
[baptiste@storage ~]$ sudo umount /mnt/data1
[baptiste@storage ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /mnt/data1 does not contain SELinux labels.
       You just mounted a file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/data1               : successfully mounted

```

