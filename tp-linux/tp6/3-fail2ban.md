# Module 3 : Fail2Ban

## 🌞 Faites en sorte que :

```
[baptiste@dp ~]$ sudo dnf install epel-release
[sudo] password for baptiste:
Extra Packages for Enterprise Linux 9 - x86_64                                                         10 MB/s |  13 MB     00:01
Rocky Linux 9 - BaseOS                                                                                7.8 kB/s | 3.6 kB     00:00
Rocky Linux 9 - AppStream                                                                              13 kB/s | 4.1 kB     00:00
Rocky Linux 9 - Extras                                                                                7.9 kB/s | 2.9 kB     00:00
Package epel-release-9-4.el9.noarch is already installed.
Dependencies resolved.
Nothing to do.
Complete!


[baptiste@dp ~]$ sudo dnf install fail2ban fail2ban-firewalld


[baptiste@dp ~]$ sudo systemctl start fail2ban
[baptiste@dp ~]$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.


[baptiste@dp ~]$ sudo systemctl status fail2ban |grep active
     Active: active (running) since Sun 2023-01-22 12:12:07 CET; 1min 46s ago

```



```
[baptiste@dp ~]$ sudo cat /etc/fail2ban/jail.local  | grep maxretry
# A host is banned if it has generated "maxretry" during the last "findtime"
# "maxretry" is the number of failures before a host get banned.
maxretry = 3


[baptiste@dp ~]$ sudo cat /etc/fail2ban/jail.local  | grep bantime
bantime      = 1h

[baptiste@dp ~]$ sudo cat /etc/fail2ban/jail.local  | grep findtime
# A host is banned if it has generated "maxretry" during the last "findtime"
findtime  = 1m

[baptiste@dp ~]$ sudo systemctl restart fail2ban
[baptiste@dp ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
     Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
     Active: active (running) since Sun 2023-01-22 12:22:14 CET; 6s ago
       Docs: man:fail2ban(1)
    Process: 12364 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
   Main PID: 12365 (fail2ban-server)
      Tasks: 3 (limit: 5905)
     Memory: 10.6M
        CPU: 98ms
     CGroup: /system.slice/fail2ban.service
             └─12365 /usr/bin/python3 -s /usr/bin/fail2ban-server -xf start

Jan 22 12:22:14 dp.tp5.linux systemd[1]: Starting Fail2Ban Service...
Jan 22 12:22:14 dp.tp5.linux systemd[1]: Started Fail2Ban Service.
Jan 22 12:22:15 dp.tp5.linux fail2ban-server[12365]: 2023-01-22 12:22:15,015 fail2ban.configreader   [12365]: WARNING 'allowipv6' not>
Jan 22 12:22:15 dp.tp5.linux fail2ban-server[12365]: Server ready

```

```
[baptiste@web ~]$ ssh baptiste@10.105.1.12
baptiste@10.105.1.12's password:
Permission denied, please try again.
baptiste@10.105.1.12's password:
Permission denied, please try again.
baptiste@10.105.1.12's password:
baptiste@10.105.1.12: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).

[baptiste@web ~]$ ssh baptiste@10.105.1.12
ssh: connect to host 10.105.1.12 port 22: Connection refused

```

```
[baptiste@dp ~]$ sudo fail2ban-client status sshd | grep Banned
`- Banned IP list:   10.105.1.11

[baptiste@dp ~]$ sudo firewall-cmd --list-all | grep rule
  rich rules:
        rule family="ipv4" source address="10.105.1.11" port port="ssh" protocol="tcp" reject type="icmp-port-unreachable"

[baptiste@dp ~]$ sudo fail2ban-client unban 10.105.1.11
1

```