# Module 1 : Reverse Proxy


## 🌞 On utilisera NGINX comme reverse proxy

```
[baptiste@proxy ~]$ sudo dnf install nginx
[sudo] password for baptiste:
Rocky Linux 9 - 
```

```
[baptiste@proxy ~]$ sudo systemctl start nginx


[baptiste@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
     Active: active (running) since Tue 2023-01-17 14:30:11 CET; 11s ago
    Process: 1006 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
```

```
[baptiste@proxy ~]$ ss -altnp
State          Recv-Q         Send-Q                   Local Address:Port                   Peer Address:Port         Process
LISTEN         0              511                            0.0.0.0:80                          0.0.0.0:*
LISTEN         0              128                            0.0.0.0:22                          0.0.0.0:*
LISTEN         0              128                               [::]:22                             [::]:*
```

```
[baptiste@proxy ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[baptiste@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanen
success

[baptiste@proxy ~]$ sudo firewall-cmd --reload
success
``` 

```
[baptiste@proxy ~]$ ps -ef | grep nginx
root        1009       1  0 14:30 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1010    1009  0 14:30 ?        00:00:00 nginx: worker process
baptiste    1102     824  0 14:42 pts/0    00:00:00 grep --color=auto nginx
```

```
[baptiste@proxy ~]$ curl localhost:80 | head -7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
100  7620  100  7620    0     0   286k      0 --:--:-- --:--:-- --:--:--  286k
curl: (23) Failed writing body
```

## 🌞 Configurer NGINX

```
[baptiste@proxy conf.d]$ sudo cat tp6.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name www.nextcloud.tp6;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.105.1.11:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}

```
---

```
[baptiste@web config]$ sudo cat config.php
<?php
$CONFIG = array (
  'instanceid' => 'oc3jqutmbjtb',
  'passwordsalt' => '70M7YMfzHVWEr4At4auvWi+KkSTnG6',
  'secret' => 'iC0G03X3PzlYMeRTQG14UGs5jjQM/BbejhaV7iIizoNZKvu+',
  'trusted_domains' =>
  array (
         0 => 'web.tp6.linux',
         1 => '10.105.1.13',
  ),
  'datadirectory' => '/var/www/tp5_nextcloud/data',
  'dbtype' => 'mysql',
  'version' => '25.0.0.15',
  'overwrite.cli.url' => 'http://web.tp5.linux',
  'dbname' => 'nextcloud',
  'dbhost' => '10.105.1.12',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'mysql.utf8mb4' => true,
  'dbuser' => 'nextcloud',
  'dbpassword' => 'pewpewpew',
  'installed' => true,
);

```

## 🌞 Faites en sorte de

```
[baptiste@web config]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[baptiste@web config]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success

[baptiste@web config]$ sudo firewall-cmd --reload
success
```

## 🌞 Une fois que c'est en place

Faire un ping manuel vers l'IP de web.tp6.linux ne fonctionne pas
```
[baptiste@proxy conf.d]$ ping 10.105.10.11
PING 10.105.10.11 (10.105.10.11) 56(84) bytes of data.
^C
--- 10.105.10.11 ping statistics ---
6 packets transmitted, 0 received, 100% packet loss, time 5018ms
```

Faire un ping manuel vers l'IP de proxy.tp6.linux fonctionne
```
[baptiste@web config]$ ping 10.105.1.13
PING 10.105.1.13 (10.105.1.13) 56(84) bytes of data.
64 bytes from 10.105.1.13: icmp_seq=1 ttl=64 time=1.50 ms
64 bytes from 10.105.1.13: icmp_seq=2 ttl=64 time=1.03 ms
64 bytes from 10.105.1.13: icmp_seq=3 ttl=64 time=1.16 ms
64 bytes from 10.105.1.13: icmp_seq=4 ttl=64 time=0.952 ms
^C
--- 10.105.1.13 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 2998ms
rtt min/avg/max/mdev = 0.952/1.160/1.501/0.210 ms
```
---
# II. HTTPS

```
[baptiste@proxy ~]$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx.key -out /etc/ssl/certs/certificate
#On génère le certificat et la clé de chiffrement.
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:
State or Province Name (full name) []:
Locality Name (eg, city) [Default City]:
Organization Name (eg, company) [Default Company Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:
Email Address []:
```

```
[baptiste@proxy ~]$ sudo cat /etc/nginx/conf.d/tp6.conf
[sudo] password for baptiste:
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name www.nextcloud.tp6;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.105.1.11:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
        listen 443 http2 ssl;
        listen [::]:443 hhtp2 ssl;
        ssl_certificate /etc/ssl/certificate;
        ssl_certificate_key /etc/ssl/private/nginx.key;
}
```

### On lui demande d'écouter sur le port convetionnel pour HTTPS : 443 en TCP

```
[baptiste@proxy ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[baptiste@proxy ~]$ sudo firewall-cmd --reload
success
[baptiste@proxy ~]$ sudo systemctl restart nginx
```