# TP 3 : We do a little scripting

## I. Script carte d'identité

### 🌞 Vous fournirez dans le compte-rendu, en plus du fichier, un exemple d'exécution avec une sortie, dans des balises de code.

```
[baptiste@tp3vm1 idcard]$ cat /srv/idcard/idcard.sh
#!/bin/bash

echo "Machine name : $(hostnamectl --static )"
echo "OS $(uname) and kernel version is $(cat /etc/redhat-release | cut -d'(' -f1)"
echo "IP : $(ip a | head -9 | tail -1 | tr -s ' ' | cut -d ' ' -f3 | cut -d '/' -f1)"
echo "RAM : $(free -h | grep Mem | tr -s ' ' | cut -d ' ' -f7) memory available on $( free -h | grep Mem | tr -s ' ' | cut -d ' ' -f2) total memory"
echo "Disk : $(df -h | grep "/$" | tr -s ' ' | cut -d ' ' -f4) space left"
echo "Top 5 process by RAM usage :
$(ps -e -o command,%mem --sort=%mem | tail -5)"
echo Listening ports :
g="$(ss -lnp4H)"
while read line ;
do
  port_type=$(cut -d' ' -f1 <<< "${line}")
  port_number=$(echo $line | cut -d' ' -f5 | cut -d':' -f2)
  process=$(echo $line |tr -s ' ' | cut -d'"' -f2)
  echo "  - ${port_number} ${port_type} ":" ${process}"
done <<< "${g}"

cat_pic=$(curl -s https://cataas.com/cat > Didier)
ext=$(file --extension Didier | cut -d' ' -f2 | cut -d'/' -f1)
if [[ ${ext} == "jpeg" ]]
then
  cat_ext="cat.${ext}"
elif [[ ${ext} == "png" ]]
then
  cat_ext="cat.${ext}"
else
  cat_ext="cat.gif"
fi
mv Didier ${cat_ext}
chmod +x ${cat_ext}

echo " "
echo "Here is your random cat : ./${cat_ext}"

```

### Un exemple d'exécution avec une sortie

```
[baptiste@tp3vm1 ~]$ sudo /srv/idcard/idcard.sh
[sudo] password for baptiste:
Machine name : tp3vm1
OS Linux and kernel version is Rocky Linux release 9.0
IP : 10.5.1.10
RAM : 644Mi memory available on 960Mi total memory
Disk : 5.1G space left
Top 5 process by RAM usage :
/usr/lib/systemd/systemd-lo  1.3
/usr/lib/systemd/systemd --  1.3
/usr/lib/systemd/systemd --  1.5
/usr/sbin/NetworkManager --  1.9
/usr/bin/python3 -s /usr/sb  4.1
Listening ports :
  - 323 udp : chronyd
  - 22 tcp : sshd

Here is your random cat : ./cat.jpeg

```

## II. Script youtube-dl

### 🌞 Vous fournirez dans le compte-rendu, en plus du fichier, un exemple d'exécution avec une sortie, dans des balises de code.

```
[baptiste@tp3vm1 yt]$ cat yt.sh

#!/bin/bash

Titre_video="$(youtube-dl -e $1)"
cd /srv/yt/downloads/
mkdir "${Titre_video}" > /dev/null
cd "${Titre_video}"
youtube-dl $1 > /dev/null
echo "Vidéo" $1 "was downloaded."
nom_vid="$(ls *.mp4)"
youtube-dl --get-description $1 > description
echo "File path : /srv/yt/downloads/""${Titre_video}""/""${nom_vid}"
cd /var/log/yt
echo "[""$(date "+%Y/%m/%d"" ""%T")""]" >> /var/log/yt/download.log
echo "Vidéo" $1 "was downloaded. File path : /srv/yt/downloads/""${Titre_video}""/""${nom_vid}" >> /var/log/yt/download.log

```

```
[baptiste@tp3vm1 yt]$ sudo /srv/yt/yt.sh https://www.youtube.com/watch?v=jNQXAC9IVRw
Vidéo https://www.youtube.com/watch?v=jNQXAC9IVRw was downloaded.
File path : /srv/yt/downloads/Me at the zoo/Me at the zoo-jNQXAC9IVRw.mp4

```

```
[baptiste@tp3vm1 yt]$ cat download.log
[2022/12/12 21:07:47]
Vidéo https://www.youtube.com/watch?v=jNQXAC9IVRw was downloaded. File path : /srv/yt/downloads/Me at the zoo/Me at the zoo-jNQXAC9IVRw.mp4
[2022/12/12 21:10:30]
Vidéo https://www.youtube.com/watch?v=Wch3gJG2GJ4 was downloaded. File path : /srv/yt/downloads/1 Second Video/1 Second Video-Wch3gJG2GJ4.mp4
```