# Partie 2 : Mise en place et maîtrise du serveur de base de données

## 🌞 Install de MariaDB sur db.tp5.linux

```ruby
[baptiste@dp ~]$ sudo dnf install mariadb-server
[sudo] password for baptiste:
Last metadata expiration check: 0:06:29 ago on Mon 09 Jan 2023 09:36:42 AM CET.
Dependencies resolved.
================================================================================
 Package                       Arch    Version                 Repository  Size
================================================================================
Installing:
 mariadb-server                x86_64  3:10.5.16-2.el9_0       appstream  9.4 M
```
---
```ruby
[baptiste@dp ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```
---
```
[baptiste@dp ~]$ sudo systemctl start mariadb
```
---
```ruby
[baptiste@dp ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] y
Enabled successfully!
Reloading privilege tables..
 ... Success!
```

```ruby
[baptiste@dp ~]$ sudo systemctl status mariadb | grep enable
     Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
```
## 🌞 Port utilisé par MariaDB

```
[baptiste@dp ~]$ ss -altnp | grep 3306
LISTEN 0      80                 *:3306            *:*
```
```ruby
[baptiste@dp ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[baptiste@dp ~]$ sudo firewall-cmd --reload
success
```

## 🌞 Processus liés à MariaDB

```
[baptiste@dp ~]$ ps -ef | grep mariadb
mysql      12850       1  0 09:47 ?        00:00:00 /usr/libexec/mariadbd --basedir=/usr
baptiste   13006     876  0 10:27 pts/0    00:00:00 grep --color=auto mariadb
```