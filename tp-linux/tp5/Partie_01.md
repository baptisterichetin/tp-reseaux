# Partie 1 : Mise en place et maîtrise du serveur Web

## 1. Installation

### 🌞 Installer le serveur Apache

``` ruby
[baptiste@web ~]$ sudo dnf upgrade --refresh
```

``` ruby
[baptiste@web ~]$ sudo dnf install httpd -y
[sudo] password for baptiste:
Rocky Linux 9 - Extras                                                                                7.0 kB/s | 2.9 kB     00:00
Rocky Linux 9 - Extras                                                                                 18 kB/s | 8.3 kB     00:00
Last metadata expiration check: 0:00:01 ago on Tue 03 Jan 2023 04:22:04 PM CET.
Dependencies resolved.
======================================================================================================================================
 Package                              Architecture              Version                            Repository                    Size
======================================================================================================================================
Installing:
 httpd  
```

###  🌞 Démarrer le service Apache

``` ruby
[baptiste@web conf]$ sudo systemctl start httpd
[baptiste@web conf]$ sudo systemctl enable httpd
```

``` ruby
[baptiste@web ~]$ ss -altnp
State          Recv-Q         Send-Q                   Local Address:Port                   Peer Address:Port         Process
LISTEN         0              128                            0.0.0.0:22                          0.0.0.0:*
LISTEN         0              128                               [::]:22                             [::]:*
LISTEN         0              511                                  *:80                                *:*
```

### 🌞 TEST

``` ruby
[baptiste@web ~]$ sudo systemctl status httpd
[sudo] password for baptiste:
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2023-01-03 16:23:05 CET; 52min ago
       Docs: man:httpd.service(8)
   Main PID: 44579 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
      Tasks: 213 (limit: 5905)
     Memory: 23.2M
        CPU: 2.221s
     CGroup: /system.slice/httpd.service
             ├─44579 /usr/sbin/httpd -DFOREGROUND
             ├─44580 /usr/sbin/httpd -DFOREGROUND
             ├─44581 /usr/sbin/httpd -DFOREGROUND
             ├─44582 /usr/sbin/httpd -DFOREGROUND
             └─44583 /usr/sbin/httpd -DFOREGROUND

Jan 03 16:23:04 web.linux.tp5 systemd[1]: Starting The Apache HTTP Server...
Jan 03 16:23:05 web.linux.tp5 systemd[1]: Started The Apache HTTP Server.
Jan 03 16:23:05 web.linux.tp5 httpd[44579]: Server configured, listening on: port 80
```

``` ruby
[baptiste@web ~]$ sudo systemctl status httpd | grep enable
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
```
``` ruby
[baptiste@web ~]$ curl localhost | head -7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
100  7620  100  7620    0     0   310k      0 --:--:-- --:--:-- --:--:--  323k
curl: (23) Failed writing body
```

``` ruby
[baptiste@web ~]$ curl 10.105.1.11:80 | head -7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
100  7620  100  7620    0     0   338k      0 --:--:-- --:--:-- --:--:--  354k
curl: (23) Failed writing body

```

## 2. Avancer vers la maîtrise du service

### 🌞 Le service Apache...

```ruby
[baptiste@web ~]$ sudo cat /usr/lib/systemd/system/httpd.service
[sudo] password for baptiste:
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target

```

### 🌞 Déterminer sous quel utilisateur tourne le processus Apache

```ruby
[baptiste@web conf]$ sudo cat httpd.conf | grep User
User apache
```

``` ruby
[baptiste@web conf]$ ps -ef | grep apache
apache     44580   44579  0 16:23 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     44581   44579  0 16:23 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     44582   44579  0 16:23 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     44583   44579  0 16:23 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
baptiste   44964     926  0 17:40 pts/0    00:00:00 grep --color=auto apache
```

```ruby
[baptiste@web testpage]$ ls -al index.html
-rw-r--r--. 1 root root 7620 Jul 27 20:05 index.html
```

### 🌞 Changer l'utilisateur utilisé par Apache

```ruby
[baptiste@web ~]$ sudo useradd servweb -d /usr/share/httpd -u 2000 -s /sbin/nologin
```

```ruby
[baptiste@web conf]$ sudo cat httpd.conf | grep User
User servweb
```

```ruby
[baptiste@web conf]$ sudo systemctl restart httpd
```

```ruby
[baptiste@web conf]$ ps -ef | grep httpd
root       45005       1  0 17:59 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
servweb    45006   45005  0 17:59 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
servweb    45007   45005  0 17:59 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
servweb    45008   45005  0 17:59 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
servweb    45009   45005  0 17:59 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
baptiste   45235     926  0 18:01 pts/0    00:00:00 grep --color=auto httpd
```
### 🌞 Faites en sorte que Apache tourne sur un autre port

```ruby
[baptiste@web conf]$ sudo cat httpd.conf | grep Listen
Listen 8080
```

```ruby
[baptiste@web conf]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[baptiste@web conf]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
[baptiste@web conf]$ sudo firewall-cmd --reload
success
[baptiste@web conf]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 8080/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

```
[baptiste@web conf]$ sudo systemctl restart httpd
```

```ruby
[baptiste@web ~]$ ss -altnp
State  Recv-Q   Send-Q    Local Address:Port    Peer Address:Port     Process
LISTEN  0       128             0.0.0.0:22           0.0.0.0:*
LISTEN  0       128                 [::]:22              [::]:*
LISTEN  0       511                    *:8080               *:*
```

```ruby
[baptiste@web ~]$ curl localhost:8080 | head -7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
100  7620  100  7620    0     0   354k      0 --:--:-- --:--:-- --:--:--  372k
curl: (23) Failed writing body
```

```ruby
[baptiste@web ~]$ curl 10.105.1.11:8080 | head -7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
100  7620  100  7620    0     0   620k      0 --:--:-- --:--:-- --:--:--  620k
curl: (23) Failed writing body
```