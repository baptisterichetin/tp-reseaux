# Partie 3 : Configuration et mise en place de NextCloud

## 1. Base de données

### 🌞 Préparation de la base pour NextCloud

```
[baptiste@dp ~]$ sudo mysql -u root -p
[sudo] password for baptiste:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 15
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.105.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.004 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.105.1.11';
Query OK, 0 rows affected (0.004 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.002 sec)
```

### 🌞 Exploration de la base de données

```
[baptiste@web ~]$ sudo dnf install mysql-8.0.30-3.el9_0.x86_64
Last metadata expiration check: 1:55:40 ago on Mon 09 Jan 2023 09:33:11 AM CET.
Dependencies resolved.
================================================================================
 Package                       Arch      Version             Repository    Size
================================================================================
Installing:
 mysql                         x86_64    8.0.30-3.el9_0      appstream    2.8 M
```

```
[baptiste@web ~]$ sudo mysql -u nextcloud -h 10.105.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 21
Server version: 5.5.5-10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```

```
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> USE nextcloud
Database changed
mysql> SHOW TABLES;
Empty set (0.01 sec)

```

### 🌞 Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données

```
[baptiste@dp ~]$ sudo mysql -u root -p
```

```
MariaDB [(none)]> select user,host from mysql.user;
+-------------+-------------+
| User        | Host        |
+-------------+-------------+
| nextcloud   | 10.105.1.11 |
| mariadb.sys | localhost   |
| mysql       | localhost   |
| root        | localhost   |
+-------------+-------------+
4 rows in set (0.001 sec)
```
---

## 2. Serveur Web et NextCloud

### 🌞 Install de PHP

```
[baptiste@web ~]$ sudo dnf config-manager --set-enabled crb
[baptiste@web ~]$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise remi-release-9.rpm -y
[baptiste@web ~]$ dnf module list php
[baptiste@web ~]$ sudo dnf module enable php:remi-8.1 -y
[baptiste@web ~]$ sudo dnf install -y php81-php
```
---
### 🌞 Install de tous les modules PHP nécessaires pour NextCloud


```
[baptiste@web ~]$  sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
```
---

### 🌞 Récupérer NextCloud

```
[baptiste@web ~]$ sudo curl https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip --output /var/www/tp5_nextcloud/nextcloud.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  168M  100  168M    0     0  26.5M      0  0:00:06  0:00:06 --:--:-- 31.1M
```

```
[baptiste@web ~]$ sudo dnf install unzip -y
[baptiste@web ~]$ unzip /var/www/tp5_nextcloud/nextcloud.zip
[baptiste@web tp5_nextcloud]$ sudo mv ~/nextcloud/* ../tp5_nextcloud/
```

```
[baptiste@web ~]$  ls -l /var/www/tp5_nextcloud/nextcloud/index.html
-rw-r--r--. 1 root root 0 Dec 12 17:09 /var/www/tp5_nextcloud/nextcloud/index.html

[baptiste@web ~]$ sudo chown -R apache:apache /var/www/tp5_nextcloud/

[baptiste@web ~]$ ls -al /var/www/tp5_nextcloud/index.html
-rw-r--r--. 1 apache apache 156 Oct  6 14:42 /var/www/tp5_nextcloud/index.html
```

---
### 🌞 Adapter la configuration d'Apache

``` ruby
[baptiste@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep Include
Include conf.modules.d/*.conf
IncludeOptional conf.d/*.conf
```

```
[baptiste@web ~]$ sudo cat /etc/httpd/conf.d/tp5.conf
<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp5_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp5.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp5_nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

### 🌞 Redémarrer le service Apache pour qu'il prenne en compte le nouveau fichier de conf

```
[baptiste@web ~]$ sudo systemctl restart httpd
```

## 3. Finaliser l'installation de NextCloud


```
MariaDB [(none)]> USE nextcloud;
Database changed
MariaDB [nextcloud]> SELECT COUNT(*) FROM information_schema.tables WHERE table_type = 'BASE TABLE';
+----------+
| COUNT(*) |
+----------+
|       95 |
+----------+
1 row in set (0.006 sec)

```
