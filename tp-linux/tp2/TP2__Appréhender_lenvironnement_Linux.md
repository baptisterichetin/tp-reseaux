# TP2 : Appréhender l'environnement Linux

## I. Service SSH

## 1. Analyse du service

### 🌞 S'assurer que le service sshd est démarré

```
[baptiste@tp2vm1 ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enable>
     Active: active (running) since Mon 2022-12-05 11:26:54 CET; 6min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 682 (sshd)
      Tasks: 1 (limit: 5905)
     Memory: 7.0M
        CPU: 128ms
     CGroup: /system.slice/sshd.service 
             └─682 "sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups"

Dec 05 11:26:54 tp2vm1 systemd[1]: Starting OpenSSH server daemon...
Dec 05 11:26:54 tp2vm1 sshd[682]: Server listening on 0.0.0.0 port 22.
Dec 05 11:26:54 tp2vm1 sshd[682]: Server listening on :: port 22.
Dec 05 11:26:54 tp2vm1 systemd[1]: Started OpenSSH server daemon.
Dec 05 11:27:09 tp2vm1 sshd[809]: Accepted password for baptiste from 10.5.1.1 port 55597>
Dec 05 11:27:10 tp2vm1 sshd[809]: pam_unix(sshd:session): session opened for user baptist>

```
---
### 🌞 Analyser les processus liés au service SSH

```
[baptiste@tp2vm1 ~]$ ps -ef | grep "sshd"
root         682       1  0 11:26 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
root         809     682  0 11:27 ?        00:00:00 sshd: baptiste [priv]
baptiste     823     809  0 11:27 ?        00:00:00 sshd: baptiste@pts/0
baptiste     873     824  0 11:53 pts/0    00:00:00 grep --color=auto sshd

```

---
### 🌞 Déterminer le port sur lequel écoute le service SSH

```
[baptiste@tp2vm1 ~]$ ss | grep "ssh"
tcp   ESTAB  0      52                       10.5.1.10:ssh           10.5.1.1:55597
```

---
### 🌞 Consulter les logs du service SSH

```
[baptiste@tp2vm1 log]$ sudo cat secure | grep sshd | tail -n 10
Dec  5 11:18:48 localhost sshd[826]: pam_unix(sshd:session): session opened for user baptiste(uid=1000) by (uid=0)
Dec  5 11:26:54 tp2vm1 sshd[682]: Server listening on 0.0.0.0 port 22.
Dec  5 11:26:54 tp2vm1 sshd[682]: Server listening on :: port 22.
Dec  5 11:27:09 tp2vm1 sshd[809]: Accepted password for baptiste from 10.5.1.1 port 55597 ssh2
Dec  5 11:27:10 tp2vm1 sshd[809]: pam_unix(sshd:session): session opened for user baptiste(uid=1000) by (uid=0)
Dec  6 09:07:24 tp2vm1 sshd[688]: Server listening on 0.0.0.0 port 22.
Dec  6 09:07:24 tp2vm1 sshd[688]: Server listening on :: port 22.
Dec  6 09:08:13 tp2vm1 sshd[814]: Accepted password for baptiste from 10.5.1.1 port 54389 ssh2
Dec  6 09:08:14 tp2vm1 sshd[814]: pam_unix(sshd:session): session opened for user baptiste(uid=1000) by (uid=0)
Dec  6 10:14:52 tp2vm1 sudo[992]: baptiste : TTY=pts/0 ; PWD=/etc/ssh ; USER=root ; COMMAND=/bin/vim sshd_config


```

## 2. Modification du service

### 🌞 Identifier le fichier de configuration du serveur SSH
```
[baptiste@tp2vm1 ssh]$ ls -al
-rw-------.  1 root root       3669 Sep 20 20:46 sshd_config

```

---
### 🌞 Modifier le fichier de conf

```
[baptiste@tp2vm1 ssh]$ echo $RANDOM
29362


[baptiste@tp2vm1 ssh]$ sudo cat sshd_config | grep Port
[sudo] password for baptiste:
Port 29362

[baptiste@tp2vm1 ssh]$ sudo firewall-cmd --list-all | grep 29362
  ports: 29362/tcp

  [baptiste@tp2vm1 ssh]$ sudo firewall-cmd --reload

```

---
### 🌞 Redémarrer le service
```
[baptiste@tp2vm1 ssh]$ sudo systemctl restart sshd

```
---
### 🌞 Effectuer une connexion SSH sur le nouveau port
```
bapti@LAPTOP-QNFQ1IVC MINGW64 ~
$ ssh baptiste@10.5.1.10 -p 29362
baptiste@10.5.1.10's password:
Last login: Tue Dec  6 11:28:51 2022 from 10.5.1.1

```

## II. Service HTTP

## 1. Mise en place

### 🌞 Installer le serveur NGINX

```
[baptiste@tp2vm1 ~]$ sudo dnf install nginx

[baptiste@tp2vm1 ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.

```

### 🌞 Démarrer le service NGINX
```
[baptiste@tp2vm1 ~]$ sudo systemctl start nginx
```

### 🌞 Déterminer sur quel port tourne NGINX

```
[baptiste@tp2vm1 ~]$ sudo ss -alnpt | grep nginx
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=812,fd=6),("nginx",pid=810,fd=6))
LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=812,fd=7),("nginx",pid=810,fd=7))


[baptiste@tp2vm1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
sucess

[baptiste@tp2vm1 ~]$ sudo firewall-cmd --reload
```

### 🌞 Déterminer les processus liés à l'exécution de NGINX
```
[baptiste@tp2vm1 ~]$ ps -ef | grep nginx
root         810       1  0 11:51 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx        812     810  0 11:51 ?        00:00:00 nginx: worker process
baptiste     928     829  0 12:12 pts/0    00:00:00 grep --color=auto nginx

```

### 🌞 Euh wait

```
[baptiste@tp2vm1 ~]$ curl http://10.5.1.10:80 | head -n 7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
100  7620  100  7620    0     0   437k      0 --:--:-- --:--:-- --:--:--  465k
curl: (23) Failed writing body

```
---
## 2. Analyser la conf de NGINX

### 🌞 Déterminer le path du fichier de configuration de NGINX

```
[baptiste@tp2vm1 nginx]$ ls -al nginx.conf
-rw-r--r--. 1 root root 2334 Oct 31 16:37 nginx.conf

```

### 🌞 Trouver dans le fichier de conf

```
[baptiste@tp2vm1 nginx]$ sudo cat nginx.conf | grep server -A 10

    server {
        listen       80;
        listen       [::]:80;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }

[baptiste@tp2vm1 nginx]$ suco cat nginx
  # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;




```

## 3. Déployer un nouveau site web

### 🌞 Créer un site web

```
[baptiste@tp2vm1 ~]$ cat /var/www/tp2_linux/index.html
<h1>MEOW mon premier serveur web</h1>
```

### 🌞 Adapter la conf NGINX