# TP1 : Are you dead yet ?

## II. Feu

### 🌞 Trouver au moins 4 façons différentes de péter la machine
---

## Première Solution :

Casser toutes les sessions en ne pouvant plus si connecter.\
Pour ce faire supprimer la "security".

```
- ce connecter en root

- cd..  "On vas à la rascine du dossier"

- ls   "ls pour voir les fichiers"

- cd lib64   "cd pour aller dans le dossier lib64"

- ls     "ls pour voir les fichiers"

- sudo rm -r security    "On supprime le fichier de sécurité"

- reboot      "On redémarre"
```
#### Après le reboot il sera impossible de se connecter même sur root.
---

## Deuxième Solution

Pour la casser supprimer le boot, ne plus dutout avoir accès au root et obliger la vm à ce mettre en mode "grub rescue>"

```
ce connecter en root

- cd..  "On vas à la rascine du dossier"

- ls   "ls pour voir les fichiers"

- sudo rm -r boot/   "je supprime tout le dossier boot"

- cd boot/    "je vais à l'intérieur"

- ls   "et ls pour vérifier qu'il n'y a plus rien a l'intérieur"

- reboot    "je redémarre et je serais envoyé sur le grub rescue car je n'ai plus d'OS"
```
---
## Troisième Solution

Pour casser la vm j'utilise une commande qui écrit instantanément des données aléatoires sur mon disque dur (/dev/sda) sans se soucier une seule seconde des systèmes de fichiers ou des partitions. 

```
Je me connecte en root

- sudo dd if=/dev/random of=/dev/sda 
```
#### Après avoir exécuté la commande elle vas modifier tout nos fichier, elle casse la vm étant donné quel supprime des fichiers cela bloque des commandes comme reboot, exit ect..
---

## Quatrième Solution

Je réalise la création d'une tache permettant que la machine s'éteigne toute seule après son démarrage.

```
- sudo crontab -e   "On ouvre le gestionnaire de tâche répété contrab"

- * * * * * /sbin/shutdown -h now  "j'ajoute une ligne au fichier permettant qu'il s'éteigne peu de temps après on démarrage"
```
---

## Cinquième Solution
Pour casser ma vm je vais bloquer le compte root et kill le processus en cours de mon root. Ce qui vas fermer mon root et je ne pourrais plus mis connecter.

```
- sudo passwd -l root  "Permet de bloquer le compte root en bloquant son password"

- sudo killall -u root     "C'est un arrêt forcé de tout les processus de root d'ou le (killall)"

"Après la dernière commande nous sommes renvoyé dans le menu login mais je ne pourrais plus me log"
```

## Sixième Solution 
Cracker le mot de passe d’un utilisateur

```
"Ce connecter en root"

- sudo cat /etc/shadow  "les hashs des utilisateurs sont stockés dans le fichier /etc/shadow"

"seuls l’utilisateur root peut accéder à ce dernier"

- sudo nano /etc/shadow

"La structure du fichier est assez simple, on a la liste des utilisateurs avec le mot de passe haché et les dates de changements de mot de passe, etc. "

"Il suffit de supprimer la première ligne ""*root" et de reboot ou exit puis il ne pourra plus ce connecter, on peut également supprimer les mots de passe des autres utilisateurs
"
```