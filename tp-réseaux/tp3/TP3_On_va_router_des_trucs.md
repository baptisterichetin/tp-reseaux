# TP3 : On va router des trucs

## 1. Echange ARP

### 🌞Générer des requêtes ARP
---
-  ping 10.3.1.12\
  PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.\
  64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=1.17 ms\
  64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.679 ms\
  64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=0.852 ms\
  64 bytes from 10.3.1.12: icmp_seq=4 ttl=64 time=0.982 ms


#### Machine : John
[baptiste@localhost ~]$ ip neigh show\
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:13 REACHABLE\
10.3.1.12 dev enp0s8 lladdr 08:00:27:d5:4d:f5 STALE

\
[baptiste@localhost ~]$ ip neigh show 10.3.1.12\
10.3.1.12 dev enp0s8 lladdr 08:00:27:d5:4d:f5 STALE


##### Prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)
[baptiste@localhost ~]$ ip neigh show 10.3.1.12\
10.3.1.12 dev enp0s8 lladdr 08:00:27:d5:4d:f5 STALE
#### Machine : Marcel
[baptiste@localhost ~]$ ip neigh show\
10.3.1.11 dev enp0s8 lladdr 08:00:27:da:be:9c STALE\
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:13 REACHABLE

\
[baptiste@localhost ~]$ ip neigh show 10.3.1.11\
10.3.1.11 dev enp0s8 lladdr 08:00:27:da:be:9c STALE



##### Prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)
[baptiste@localhost ~]$ ip a\
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000\
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00\
    inet 127.0.0.1/8 scope host lo\
       valid_lft forever preferred_lft forever\
    inet6 ::1/128 scope host\
       valid_lft forever preferred_lft forever\
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000\
    link/ether 08:00:27:d5:4d:f5 brd ff:ff:ff:ff:ff:ff\
    inet 10.3.1.12/24 brd 10.3.1.255 scope global noprefixroute enp0s8\
       valid_lft forever preferred_lft forever\
    inet6 fe80::a00:27ff:fed5:4df5/64 scope link\
       valid_lft forever preferred_lft forever\

---
## 2. Analyse de trames
### 🌞Analyse de trames

[baptiste@localhost ~]$ sudo tcpdump -i enp0s8 -c 10 -w tcpdump.pcap\
dropped privs to tcpdump\
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), snapshot length 262144 bytes\
10 packets captured\
10 packets received by filter\
0 packets dropped by kernel

scp baptiste@10.3.1.12:/home/baptiste/tcpdump.pcap ./

[ma capture pcap : t enpscpdump](./tcpdump.pcap)

[baptiste@localhost ~]$ sudo ip neigh flush all

##### ping de john vers Marcel
```
- [baptiste@localhost ~]$ ping 10.3.1.12\
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.\
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=1.62 ms\
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.901 ms\
64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=0.935 ms\
64 bytes from 10.3.1.12: icmp_seq=4 ttl=64 time=0.882 ms\
64 bytes from 10.3.1.12: icmp_seq=5 ttl=64 time=0.885 ms\
64 bytes from 10.3.1.12: icmp_seq=6 ttl=64 time=0.436 ms\
64 bytes from 10.3.1.12: icmp_seq=7 ttl=64 time=0.445 ms\
64 bytes from 10.3.1.12: icmp_seq=8 ttl=64 time=0.749 ms\
64 bytes from 10.3.1.12: icmp_seq=9 ttl=64 time=0.389 ms\
64 bytes from 10.3.1.12: icmp_seq=10 ttl=64 time=0.906 ms\
64 bytes from 10.3.1.12: icmp_seq=11 ttl=64 time=0.913 ms\
--- 10.3.1.12 ping statistics ---\
11 packets transmitted, 11 received, 0% packet loss, time 10138ms\
rtt min/avg/max/mdev = 0.389/0.823/1.617/0.324 ms
```


[ma capture pcap : ARP](./tp3_arp.pcap)

---
## II. Routage
### 1. Mise en place du routage
### 🌞Activer le routage sur le noeud router
$ sudo firewall-cmd --list-all
public (active)

$ sudo firewall-cmd --get-active-zone\
public \
   interfaces : enps0s9 enp0s8

$ sudo firewall-cmd --add-masquerade --zone=public\
success

$ sudo firewall-cmd --add-masquerade --zone=public --permanent\
success


### 🌞Ajouter les routes statiques nécessaires pour que john et marcel puissent se ping

#### Ajout route depuis john 
$ sudo ip route add 10.3.2.0/24 via 10.3.1.254 dev enp0s8
#### Ajout route depuis Marcel
$ sudo ip route add 10.3.1.0/24 via 10.3.2.254 dev enp0s9

Ping :
```
[baptiste@localhost ~]$ ping 10.3.2.12\
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.\
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=1.33 ms\
64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=1.77 ms\
64 bytes from 10.3.2.12: icmp_seq=3 ttl=63 time=1.76 ms\

--- 10.3.2.12 ping statistics ---\
3 packets transmitted, 3 received, 0% packet loss, time 2004ms\
rtt min/avg/max/mdev = 1.334/1.622/1.770/0.203 ms
```
---
### 2. Analyse de trames

| ordre | type trame  | IP source | MAC source              | IP destination | MAC destination            |
|-------|-------------|-----------|-------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         | `marcel` `08:00:27:b7:ef:55` | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         |    `0a:00:27:00:00:40`                       | x              | `marcel` `08:00:27:b7:ef:55`    |
| 3   |   Requête ARP       |   x     |   `routeur` `08:00:27:53:4f:a6`                   |      x          |              Broadcast `FF:FF:FF:FF:FF`           |
4   |   Réponse ARP       |   x     |      `marcel` `08:00:27:b7:ef:55`                |         x       |             `routeur` `08:00:27:53:4f:a6`               |
|    5   |   Requête ARP       |   x     |   `marcel` `08:00:27:b7:ef:55`                   |      x          |              `routeur` `08:00:27:53:4f:a6`           |
6   |   Réponse ARP       |   x     |      `routeur` `08:00:27:53:4f:a6`                |         x       |             `marcel` `08:00:27:b7:ef:55`               |
|7 | Ping        | `10.3.2.254`        |`routeur` `08:00:27:53:4f:a6`                       | `10.3.2.12`             | `marcel : 08:00:27:b7:ef:55`                          |
|   8  | Pong        | `10.3.2.12`         | `marcel` `08:00:27:b7:ef:55`                       |       `10.3.2.254`        | `08:00:27:53:4f:a6`           

[ma capture pcap : Marcel](./tp3_routage_marcel.pcap)

---
###  3. Accès internet
### 🌞Donnez un accès internet à vos machines
\
#### Pour John : $ sudo nano /etc/sysconfig/network
GATEWAY=10.3.1.254\
$ sudo systemctl restart NetworkManager

#### Pour Marcel : $ sudo nano /etc/sysconfig/network
GATEWAY=10.3.1.254\
$ sudo systemctl restart NetworkManager

\
Teste Ping :
```
[baptiste@localhost ~]$ ping 8.8.8.8\
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.\
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=46.8 ms\
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=37.5 ms\
^C\
--- 8.8.8.8 ping statistics ---\
2 packets transmitted, 2 received, 0% packet loss, time 1001ms\

\
[baptiste@localhost ~]$ ping google.com\
PING google.com (216.58.214.78) 56(84) bytes of data.\
64 bytes from fra15s10-in-f14.1e100.net (216.58.214.78): icmp_seq=1 ttl=112 time=24.0 ms
```

---
#### Donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser :
#### Sur le routeur
[baptiste@localhost ~]$ sudo nano /etc/resolv.conf\
\
google.com 8.8.8.8

\
[baptiste@localhost ~]$ curl gitlab.com

\
[baptiste@localhost ~]$ dig gitlab.com
```
; <<>> DiG 9.16.23-RH <<>> gitlab.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 5192
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;gitlab.com.                    IN      A

;; ANSWER SECTION:
gitlab.com.             300     IN      A       172.65.251.78

;; Query time: 41 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Fri Oct 28 11:47:19 CEST 2022
;; MSG SIZE  rcvd: 55

```
--- 
### 🌞Analyse de trames

| ordre | type trame | IP source          | MAC source              | IP destination | MAC destination |     |
|-------|------------|--------------------|-------------------------|----------------|-----------------|-----|
| 1     | ping       | `10.3.1.11` | `John` `08:00:27:da:be:9c` | `8.8.8.8`      | `08:00:27:dc:ad:cd`             |     |
| 2     | pong       | `8.8.8.8`               | `08:00:27:dc:ad:cd`                     |      `10.3.1.11`       |     `John` `08:00:27:da:be:9c`       |  |


[ma capture pcap : Ping 8.8.8.8](./tp3_routage_internet.pcap)

---
## III. DHCP

### 🌞Sur la machine john, vous installerez et configurerez un serveur DHCP (go Google "rocky linux dhcp server").

#### Installation du serveur sur john

```
sudo nano /etc/dhcp/dhcpd.conf

default-lease-time 900;
max-lease-time 10800;

authoritative;

subnet 10.0.5.15 netmask 255.255.255.0 {
range 10.0.5.50 10.0.5.99;
option routers 10.0.5.1;
option subnet-mask 255.255.255.0;
option domain-name-servers 10.0.5.1;
}
```

#### Créer une machine bob

faites lui récupérer une IP en DHCP à l'aide de votre serveur

utilisez le mémo toujours, section "Définir une IP dynamique (DHCP)"

```
sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8

DEVICE=enp0s8

BOOTPROTO=dhcp
ONBOOT=yes

sudo nmcli con reload

sudo nmcli con up "System enp0s8"

sudo systemctl restart NetworkManager

```