# TP4 : TCP, UDP et services réseau

## I. First steps

### 🌞 Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP

Overwatch : 
```
PS C:\Windows\system32> netstat -b -n -p udp -a
 UDP    0.0.0.0:50033          *:*
 [Overwatch.exe]
```

[ma capture Overwatch](./packet_overwatch.pcapng)

---

Fortnite :
```
PS C:\Windows\system32> netstat -b -n -p udp -a
UDP    0.0.0.0:53055          *:*
 [System]
 ```

 [ma capture Fortnite](./packet_fortnite.pcapng)

 ---

 Rocket League :
```
PS C:\Windows\system32> netstat -b -n -p udp -a
UDP    0.0.0.0:27016          *:*
 [RocketLeague.exe]
```
[ma capture Rocket League](./packet_rocket.pcapng)

---

Discord :
```
PS C:\Windows\system32> netstat -b -n -p udp -a
UDP    0.0.0.0:64084          *:*
 [Discord.exe]
```

[ma capture Discord](./packet_discord.pcapng)

---
Minecraft :
```
PS C:\Windows\system32> netstat -b -n
  TCP    10.33.16.234:50460     209.222.114.25:25565   ESTABLISHED
 [javaw.exe]
 ```

 [ma capture Minecraft](./packet_minecraft.pcapng)

---

 ## II. Mise en place
### 1. SSH

### 🌞 Examinez le trafic dans Wireshark

#### Déterminez si SSH utilise TCP ou UDP

```
PS C:\Windows\system32> netstat -p tcp -n -b

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    10.4.1.1:50086         10.4.1.11:22           ESTABLISHED
 [ssh.exe]
 ```

 [ 3-way handshake](./3-wayhandshake.pcapng)

---
### 🌞 Demandez aux OS

```
bapti@LAPTOP-QNFQ1IVC MINGW64 ~
$ netstat

Connexions actives

  Proto  Adresse locale         Adresse distante       ▒tat
  TCP    10.4.1.1:50334         node1:ssh              ESTABLISHED

```

```
 [baptiste@node1 ~]$ ss
 tcp     ESTAB      0    0    10.4.1.11:ssh      10.4.1.1:50318
```
---
### 2. Routage
---

### III. DNS
#### 2. Setup

### 🌞 Dans le rendu, je veux
### Un cat des fichiers de conf

```
[baptiste@localhost ~]$ sudo cat /etc/named.conf
//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//

options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { localhost; any; };
        allow-query-cache { localhost; any; };
        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion yes;

        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";
        geoip-directory "/usr/share/GeoIP";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
};

# référence vers notre fichier de zone
zone "tp4.b1" IN {
     type master;
     file "tp4.b1.db";
     allow-update { none; };
     allow-query {any; };
};
# référence vers notre fichier de zone inverse
zone "1.4.10.in-addr.arpa" IN {
     type master;
     file "tp4.b1.rev";
     allow-update { none; };
     allow-query { any; };
};


logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
        type hint;
        file "named.ca";
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";

```

```
[baptiste@localhost ~]$ sudo cat /var/named/tp4.b1.db
$TTL 86400
@ IN SOA dns-server.tp4.b1. admin.tp4.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns-server.tp4.b1.

; Enregistrements DNS pour faire correspondre des noms à des IPs
dns-server IN A 10.4.1.201
node1      IN A 10.4.1.11

```

```
[baptiste@localhost ~]$ sudo cat /var/named/tp4.b1.rev
$TTL 86400
@ IN SOA dns-server.tp4.b1. admin.tp4.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns-server.tp4.b1.

;Reverse lookup for Name Server
201 IN PTR dns-server.tp4.b1.
11 IN PTR node1.tp4.b1.

```

### Un systemctl status named qui prouve que le service tourne bien

```
[baptiste@localhost ~]$ systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
     Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-11-10 11:56:38 CET; 13min ago
   Main PID: 34137 (named)
      Tasks: 5 (limit: 5905)
     Memory: 19.3M
        CPU: 116ms
     CGroup: /system.slice/named.service
             └─34137 /usr/sbin/named -u named -c /etc/named.conf

Nov 10 11:56:38 dns-server.tp4.b1 named[34137]: zone tp4.b1/IN: loaded serial 2019061800
Nov 10 11:56:38 dns-server.tp4.b1 named[34137]: zone 1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa/IN: loa>
Nov 10 11:56:38 dns-server.tp4.b1 named[34137]: zone localhost.localdomain/IN: loaded serial 0
Nov 10 11:56:38 dns-server.tp4.b1 named[34137]: zone localhost/IN: loaded serial 0
Nov 10 11:56:38 dns-server.tp4.b1 named[34137]: all zones loaded
Nov 10 11:56:38 dns-server.tp4.b1 systemd[1]: Started Berkeley Internet Name Domain (DNS).
Nov 10 11:56:38 dns-server.tp4.b1 named[34137]: running
Nov 10 11:56:38 dns-server.tp4.b1 named[34137]: network unreachable resolving './DNSKEY/IN': 2001:503:c27::2:30#53
Nov 10 11:56:38 dns-server.tp4.b1 named[34137]: managed-keys-zone: Initializing automatic trust anchor management for zone '.'; DNSKE>
Nov 10 11:56:38 dns-server.tp4.b1 named[34137]: resolver priming query complete
lines 1-20/20 (END)

```

### Une commande ss qui prouve que le service écoute bien sur un port
```
[baptiste@localhost ~]$ sudo ss -ltunp
Netid     State      Recv-Q     Send-Q         Local Address:Port         Peer Address:Port    Process
udp       UNCONN     0          0                  127.0.0.1:323               0.0.0.0:*        users:(("chronyd",pid=651,fd=5))
udp       UNCONN     0          0                 10.4.1.201:53                0.0.0.0:*        users:(("named",pid=34137,fd=19))
udp       UNCONN     0          0                  127.0.0.1:53                0.0.0.0:*        users:(("named",pid=34137,fd=16))
udp       UNCONN     0          0                      [::1]:323                  [::]:*        users:(("chronyd",pid=651,fd=6))
udp       UNCONN     0          0                      [::1]:53                   [::]:*        users:(("named",pid=34137,fd=22))
tcp       LISTEN     0          10                10.4.1.201:53                0.0.0.0:*        users:(("named",pid=34137,fd=21))
tcp       LISTEN     0          10                 127.0.0.1:53                0.0.0.0:*        users:(("named",pid=34137,fd=17))
tcp       LISTEN     0          128                  0.0.0.0:22                0.0.0.0:*        users:(("sshd",pid=682,fd=3))
tcp       LISTEN     0          4096               127.0.0.1:953               0.0.0.0:*        users:(("named",pid=34137,fd=24))
tcp       LISTEN     0          10                     [::1]:53                   [::]:*        users:(("named",pid=34137,fd=23))
tcp       LISTEN     0          128                     [::]:22                   [::]:*        users:(("sshd",pid=682,fd=4))
tcp       LISTEN     0          4096                   [::1]:953                  [::]:*        users:(("named",pid=34137,fd=25))
```

### 🌞 Ouvrez le bon port dans le firewall

```
[baptiste@localhost ~]$ sudo firewall-cmd --add-port=53/udp --permanent
success

[baptiste@localhost ~]$ sudo firewall-cmd --remove-port=53/tcp --permanent
success
[baptiste@localhost ~]$ sudo firewall-cmd --reload
success

```
### 3. Test

#### 🌞 Sur la machine node1.tp4.b1
```
nameserver[baptiste@node1 ~]$ sudo nano /etc/resolv.conf

[sudo] password for baptiste:
 10.4.1.201
```
---
```
[baptiste@node1 ~]$ dig node1.tp4.b1

; <<>> DiG 9.16.23-RH <<>> node1.tp4.b1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 36813
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: ce4fc6f1213b955201000000636ce3274fc94597bc11679e (good)
;; QUESTION SECTION:
;node1.tp4.b1.                  IN      A

;; ANSWER SECTION:
node1.tp4.b1.           86400   IN      A       10.4.1.11

;; Query time: 0 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Thu Nov 10 12:40:23 CET 2022
;; MSG SIZE  rcvd: 85

```
---
```
[baptiste@node1 ~]$ dig dns-server.tp4.b1

; <<>> DiG 9.16.23-RH <<>> dns-server.tp4.b1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 53167
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 4ef20d5939255ea501000000636ce352102d8f325a640911 (good)
;; QUESTION SECTION:
;dns-server.tp4.b1.             IN      A

;; ANSWER SECTION:
dns-server.tp4.b1.      86400   IN      A       10.4.1.201

;; Query time: 1 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Thu Nov 10 12:41:06 CET 2022
;; MSG SIZE  rcvd: 90

```

---
```
[baptiste@node1 ~]$ dig www.google.com

; <<>> DiG 9.16.23-RH <<>> www.google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 60273
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: fe7e9ef374c1c15601000000636ce37c29c3395c69f25a54 (good)
;; QUESTION SECTION:
;www.google.com.                        IN      A

;; ANSWER SECTION:
www.google.com.         300     IN      A       216.58.214.164

;; Query time: 482 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Thu Nov 10 12:41:48 CET 2022
;; MSG SIZE  rcvd: 87

```

### 🌞 Sur votre PC