#   TP1- Premier pas réseau

## I. Exploration locale en solo
### 1. Affichage d'informations sur la pile TCP/IP locale
### 🌞Affichez les infos des cartes réseau de votre PC
Commande utilisé : Ipconfig /all
####  L'interface WiFi :
 - nom : Intel(R) Wi-Fi 6 AX201 160MHz
 Ip : 10.33.19.174
 Mac : 8C-B8-7E-9A-B8-5C
 #### L'interface Ethernet
 - nom : Ethernet Ethernet
 Ip : pas d'ip présente
 Mac : 08-8F-C3-4B-CC-81

### 🌞Affichez votre gateway
Commande utilisé : Ipconfig /all
Passerelle : 10.33.19.254

###  🌞Déterminer la MAC de la passerelle
Commande utilisé : arp -a
La commande arp permet la consultation et parfois la modification de la table ARP dans certains systèmes d'exploitation
Mac : 00-c0-e7-e0-04-4e

#### 🌞Trouvez comment afficher les informations sur une carte IP (change selon l'OS)
Accéder au panneau de configuration, puis sélectionner "Réseau et Internet" et ensuite "Centre Réseau et Partage".
Accéder a notre carte Wifi nommé "Wifi@YNOV", une page s'ouvre et appuyé sur détails, une autre page s'ouvre avec toute les informations voulu l'IP, la MAC te la Gateway.
### 2. Modifications des informations
#### A. Modification d'adresse IP (part 1)
####  🌞Utilisez l'interface graphique de votre OS pour changer d'adresse IP 
- Panneau de configuration
Réseau et Internet
Centre Réseau et partage
Wi-fi(Wifi@YNOV)
Propriété 
"Protocole Intenet version 4(TCP/IPV4)"
"Utiliser l'adresse IP suivante :" entre l'adresse que je veux en respectant les critères.
J'ai entré comme adresse Ip : 10.33.19.10

#### 🌞Il est possible que vous perdiez l'accès internet.
C'est possible car nous nous connectons sur une adresse qui peut déja appartenir à quelqu'un, cette action n'affecte que nous en n'ayant plus d'accès à Internet.
Pour revenir normalement il suffit de re sélectionné "obtenir une adresse Ip automatiquement"

### 3. Modification d'adresse IP
####  🌞Modifiez l'IP des deux machines pour qu'elles soient dans le même réseau
- Connecter nos deuc ordinateurs en RJ45
- Comme à l'étape précédente je modifie manuelle l'adresse IP
- Je me met sur le même réseau en mettant 10.10.10.1 et 10.10.10.2
- Mettre le masque de sous-réseau à /24 soit 255.255.255.0

#### 🌞 Vérifier à l'aide d'une commande que votre IP a bien été changée
Réalisé un Ipconfig /all et vérifié que l'IP soit bien présente dans la carte Ethernet

#### 🌞Vérifier que les deux machines se joignent

- puis ping vers l'IP de l'autre ordinateur
- ping 10.10.10.1
- Réponse renvoyé par le terminal

#### 🌞Déterminer l'adresse MAC de votre correspondant
Commande à utilisé : arp -a
La commande affiche l'IP de mon correspondant qui est 10.10.10.1 et donc je peux récupéré le mac de son ip qui est  
"50-a0-30-02-ef-59"
### 4. Utilisation d'un des deux comme gateway
#### 🌞Tester l'accès internet
je réalise un ping 1.1.1.1 pour vérifié la connectivité à internet.
Le teste est fonctionnel.

#### 🌞Prouver que la connexion Internet passe bien par l'autre PC
Si nous fessons un traceroute vers 1.1.1.1 il renvoie dans le chemin d'accès notre gateway(passerelle)

### 5. Petit chat privé
#### Sur le PC serveur
Installer netcat
On créer un numéro de port avec la commande nc.exe -l -p 8888, le port est 8888 et devons ensuite attendre que le client ce connecte pour établir une connexion.
Le "l" signifie l'écoute et le port pour désigné un port.

#### 🌞Sur le PC client
Installer netcat
Le Client ce connecte a l'ip du serveur et au port qu'a désigné le serveur, a ce moment la notre client et serveur comunique il peuvent ce parler.

 #### 🌞Visualiser la connexion en cours

Avec la commande "netstat -a -n -b" le serveur visualise la connexion tcp en renvoyant l'ip et le port du serveur ainsi que l'IP et le port du client connecté et indique que la connexion est établie.

#### 🌞Pour aller un peu plus loin
Elle indique que le serveur écoute en attendant qu'un client ce connect
- nc.exe -1 -p "le numero de port" -s "IP_ADDRESS"
le serveur vas dire que toute les personne ayant la connaisance du port ici 8888 peut si connecter que si ils ont l IP

- nc.exe -1 -p "le numero de port"
alors qu en effectuer la commande sans le -s tout le monde peut si connecter grace au wifi apres en verifient 
nous verrons que notre adresse IP est a 0.0.0.8888, on indique l'IP car sinon tout le monde pourrait ce connecter directement en Wifi depuis le port 8888.

### 6. Firewall

#### Activez et configurez votre firewall

#### 🌞autoriser les ping
- Appuyez sur Démarrer, tapez «pare-feu Windows avec», puis lancez «Pare-feu Windows avec sécurité avancée».
- Vous allez créer une nouvelle règles: une pour autoriser les requêtes ICMPv4
- Vous allez créer deux nouvelles règles: une pour autoriser les requêtes ICMPv4
- Dans la fenêtre « Assistant Nouvelle règle entrante », sélectionnez « Personnalisé », puis cliquez sur « Suivant »
- Sur la page suivante, assurez-vous que «Tous les programmes» est sélectionné, puis cliquez sur «Suivant».
- Sur la page suivante, choisissez «ICMPv4» dans la liste déroulante «Type de protocole», puis cliquez sur le bouton «Personnaliser».
- Dans la fenêtre «Personnaliser les paramètres ICMP», sélectionnez l’option «Types ICMP spécifiques». Dans la liste des types ICMP, activez « Demande d’écho », puis cliquez sur « OK ».
- De retour dans la fenêtre « Assistant Nouvelle règle entrante », vous êtes prêt à cliquer sur « Suivant ».
- Sur la page suivante, il est plus simple de s’assurer que les options «Toute adresse IP» sont sélectionnées pour les adresses IP locales et distantes puis faite suivant
- Sur la page suivante, assurez-vous que l’option « Autoriser la connexion » est activée, puis cliquez sur « Suivant ».
- La page suivante vous permet de contrôler le moment où la règle est active- Tous sélectionner et suivant
- Et enfin donner le nom que l'on souhaite donc ping ICMP

#### 🌞autoriser le traffic sur le port qu'utilise nc

- Retourner au même endroit pour créer une nouvelle règle dans le trafic entrant
- Dans la page Type de règle de l’Assistant Nouvelle règle de trafic entrant, cliquez sur Personnalisé, puis sur Suivant.
- Dans la page Programme , cliquez sur Tous les programmes, puis sur Suivant
- Dans la page Protocole et ports , sélectionnez le type de protocole que vous souhaitez autoriser. Pour limiter la règle à un numéro de port spécifié, vous devez sélectionner TCP ou UDP. Dans notre cas nous pouvons spécifié notre port et alors une echelle de port.
- Une fois que vous avez configuré les protocoles et les ports, cliquez sur Suivant.
- Dans la page Étendue cliquez sur Suivant
- Dans la page Action , sélectionnez Autoriser la connexion, puis cliquez sur Suivant.
- Dans la page Profil , sélectionnez les types d’emplacement réseau auxquels cette règle s’applique, puis cliquez sur Suivant. Pour nous on sélectionne tous.
- Dans la page Nom , tapez un nom et une description pour votre règle, puis cliquez sur Terminer, Dans notre cas je l'ai appelé TCP.

### III. Manipulations d'autres outils/protocoles côté client

#### 1. DHCP

#### 🌞Exploration du DHCP, depuis votre PC

En réalisant la commande "Ipconfig /all" j'obtiens l'adresse Ip de mon serveur DHCP qui est "10.33.19.254"
Le bail obtenue est la date et l'heure a la quel je me suis connecté au réseaux dans mon cas le "Bail obtenu. . . . . . . . . . . . . . : jeudi 6 octobre 2022 07:51:21" et ensuite le Bail expirant vas vérifier si je suis toujours et vas changé tes IP, ça durée dans notre serveur est de 24h, de même le bail expirant indique "Bail expirant. . . . . . . . . . . . . : vendredi 7 octobre 2022 07:51:21"

### 2. DNS
#### ** Trouver l'adresse IP du serveur DNS que connaît votre ordinateur

Mes adresses IP de mon DNS sont 8.8.8.8, 8.8.4.4, 1.1.1.1 si l'un d'entre elle ne répond pas alors elle interroge la suivante, pour la trouvé j'ai réalis

#### 🌞Utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

- je réalise les commande "nslookup google.com et nslookup ynov.com"
Dans les deux résultat il interroge l'adresse Ip 8.8.8.8, et renvoie ensuite leur adresse ip et ils interrogent tout deux le serveur de google donc le dns.google
- Avec le reverse nslookup on interroge deux adresses, à l'inverse le reverse interroge l'ip et renvoie le nom de domaine.
- pour l'adresse 231.34.113.12 il ne renvoie rien car ne trouve rien
- pour l'adresse 78.34.2.17 il renvoie un nom de domaine qui correspondait a l'adresse

### IV. Wireshark
#### 1. Intro Wireshark
#### 🌞Utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :
- Je ping 1.1.1.1 et observe sur wireshark les request envoyés par mon IP et les reply de 1.1.1.1.
![](https://i.imgur.com/E0ruhUB.png)
- Ensuite je teste avec mon mate voir la communication![](https://i.imgur.com/6HPIzQk.png)
- Et enfin un lookup pour observer une requête DNS.![](https://i.imgur.com/OkcccQD.png)


#### 🌞 Wireshark it
![](https://i.imgur.com/Z4PAdAc.png)



