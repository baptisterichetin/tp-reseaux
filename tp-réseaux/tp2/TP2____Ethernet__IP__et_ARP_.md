# TP2  : Ethernet, IP, et ARP

## I. Setup IP

### 🌞 Mettez en place une configuration réseau fonctionnelle entre les deux machines
- Pour réalisé la configuration réseau je suis passé par powershell en l'éxécutant en tant que administrateur.
- on utilise la commande "netsh" qui permet d'accèder à la configuration réseau
- Je spécifie "Interface Ip" et ce la vas changer la console pour "netsh interface ipv4"
- Une fois dans la bonne console j'utilise la commande "set address “Ethernet0” static 192.168.1.10 255.255.255.0 192.168.1.1" qui modifie mon IP en IP static, le masque et le gateway et choisisons le nom de la carte réseau a modifier
- Pour vérifier "Ipconfig /all" j'observe bien ma config modifier
### 🌞 Prouvez que la connexion est fonctionnelle entre les deux machines


Lorsque notre configuration est finis, je ping mon correspondant et j'ai une réponse de ça part

### 🌞 Wireshark it

Le type de ICMP est un type 8.

[ma capture pcap](./Paquet_ICMP.pcapng)

## II. ARP my bro
### 🌞 Check the ARP table

- Le MAC de mon binôme est "00-0e-c6-4c-e9-10", pour le trouvé je l'ai ping puis est fait la commande arp -a
- Le MAC de la gateway est 00-c0-e7-e0-04-4e, commande arp -a

### 🌞 Manipuler la table ARP
- Vider le cache commande : arp -d
- arp -a, chaque interface contiennent très peu d'échange d'adresse IP.
- ping vers 192.165.1.11, arp -a, il y a eu un échange d'adresse IP que l'on constate dans la table arp

### 🌞 Wireshark it
Pour la première trame, souce : MAC de mon IP et destination : Broadcast
Pour la deuxième trame, source : MAC de l'IP de mon mate et destination: MAC de mon IP.

[ma capture pcap](./arp wireshark.pcapng)

## II.5 Interlude hackerzz

## III. DHCP you too my brooo

- Les quatres trqames que contient DORA sont les trames Discover, Offer, Request, Ack.
- Discover source: 0.0.0.0 et destination : 255.255.255.255
- Offer source : 10.33.19.254 et 10.33.16.234
- Request source : 0.0.0.0 et destination 255.255.255.255
- ACK source : 10.33.19.254 et destination 10.33.16.234

- 1. Mon IP est 10.33.16.234
- 2. Ma passerelle est 10.33.19.254
- 3. Serveur DNS est 8.8.8.8 , 8.8.4.4 et 1.1.1.1

[ma capture pcap](./Dora.pcapng)